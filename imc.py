import pandas as pd
import os

# Obtém o diretório atual do script em execução
diretorio_atual = os.path.dirname(os.path.abspath(__file__))

# Constrói o caminho completo para o arquivo DATASET.csv
caminho_arquivo = os.path.join(diretorio_atual, 'DATASET.csv')

dataset = pd.read_csv(caminho_arquivo, encoding='utf-8', sep= ';')

for coluna in dataset.columns.values:
    dataset[coluna]= dataset[coluna].apply(lambda value:str(value)
        .strip()
        .upper()
        .replace(',','.')
    )

dataset['Sobrenomes']= dataset['Sobrenomes'].replace(r'\s+', ' ', regex=True)
dataset['Nome completo']= dataset['Primeiro Nome'] + ' ' + dataset['Sobrenomes']
dataset['Peso (kg)']=dataset['Peso (kg)'].astype(float)
dataset['Altura (m)']=dataset['Altura (m)'].astype(float)


for index in dataset.index:
    if dataset.iloc[index,2] < dataset.iloc[index,3]:
        a=dataset.iloc[index,2]
        dataset.iloc[index,2]=dataset.iloc[index,3]
        dataset.iloc[index,3]=a


       
dataset['IMC']= dataset['Peso (kg)']/(dataset['Altura (m)']*dataset['Altura (m)'])
dataset=dataset.round(2)

 
dataset1=pd.DataFrame()
dataset1['Nome completo']=dataset['Nome completo'] + " " + dataset['IMC'].astype(str)
dataset1['Nome completo']= dataset1['Nome completo'].apply(lambda value:str(value).replace('nan','FALTAM DADOS'))

print(dataset1.to_string(index=False))

dataset1.to_csv('LucasdeJesusArruda.txt', sep=' ', encoding="utf-8", index=False, header=False)
