package com.imc;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Dataset {
    private List<Pessoa> pessoas;

    public Dataset() {
        this.pessoas = new ArrayList<>();
    }

    public void carregarDados(String caminho) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(caminho);
        if (inputStream == null) {
            throw new FileNotFoundException("Arquivo não encontrado: " + caminho);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        String linha;
        // Ignorar a primeira linha (cabeçalhos)
        br.readLine();
        while ((linha = br.readLine()) != null) {
            String[] campos = linha.split(";");
            if (campos.length == 4) {
                String primeiroNome = campos[0].trim().toUpperCase();
                String sobrenomes = campos[1].trim().toUpperCase().replaceAll("\\s+", " ");
                double peso = Double.parseDouble(campos[2].trim().replace(',', '.'));
                double altura = Double.parseDouble(campos[3].trim().replace(',', '.'));
                Pessoa pessoa = new Pessoa(primeiroNome, sobrenomes, peso, altura);
                pessoas.add(pessoa);
            }
        }
        br.close();
    }

    public void salvarDados(String caminho) throws IOException {
        FileWriter fw = new FileWriter(caminho);
        for (Pessoa pessoa : pessoas) {
            String nomeCompleto = pessoa.getNomeCompleto();
            double imc = pessoa.getIMC();
            String linha = nomeCompleto + " " + (imc > 0 ? String.format("%.2f", imc) : ":FALTAM DADOS") + "\n";
            fw.write(linha);
        }
        fw.close();
    }

    public void ajustarPesoAltura() {
        for (Pessoa pessoa : pessoas) {
            if (pessoa.getPeso() < pessoa.getAltura()) {
                double temp = pessoa.getPeso();
                pessoa.setPeso(pessoa.getAltura());
                pessoa.setAltura(temp);
            }
        }
    }
}
