package com.imc;

public class Pessoa {
    private String primeiroNome;
    private String sobrenomes;
    private double peso;
    private double altura;
    private double imc;

    public Pessoa(String primeiroNome, String sobrenomes, double peso, double altura) {
        this.primeiroNome = primeiroNome;
        this.sobrenomes = sobrenomes;
        this.peso = peso;
        this.altura = altura;
        calcularIMC();
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getSobrenomes() {
        return sobrenomes;
    }

    public void setSobrenomes(String sobrenomes) {
        this.sobrenomes = sobrenomes;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
        calcularIMC();
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
        calcularIMC();
    }

    public double getIMC() {
        return imc;
    }

    private void calcularIMC() {
        if (altura > 0) {
            this.imc = peso / (altura * altura);
        } else {
            this.imc = 0;
        }
    }

    public String getNomeCompleto() {
        return primeiroNome + " " + sobrenomes;
    }
}
