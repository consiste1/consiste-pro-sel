package com.imc;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        Dataset dataset = new Dataset();
        try {
            dataset.carregarDados("DATASET.csv");
            dataset.ajustarPesoAltura();
            dataset.salvarDados("LucasdeJesusArruda.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
